+++
author = "Aguacate anónimo"
title = "Unconference DevRel España"
date = "2020-02-13"
description = "Agenda e información útil"
tags = [
    "unconference",
]
+++

# Cúando

El próximo Sábado 22 de Febrero, a patir de las 9:30 de la mañana.

# Dónde

[Oficinas Liferay España](https://goo.gl/maps/ndN3AQEB7brCyvHk6) 

# Código de Conducta

* Ni slideshows ni presentaciones
* Tus egos, déjalos en casa
* Como código de conducta, seguiremos el [Contributor Covenant CoC](https://www.contributor-covenant.org/version/2/0/code_of_conduct/)

# Agenda
 
Hora (tentativa) | Topic
-----------------|-------------------
09:30 - 10:00    | Recepción
10:00 - 10:30    | Intro y presentaciones
10:30 - 11:30    | Grupos de conceptualización
11:30 - 12:00    | Pausa café
12:00 - 13:30    | Grupos de desarrollo
13:30 - 14:15    | Pausa / Comida
14:15 - 15:30    | Grupos de terminación
15:30 - 16:00    | Resultados y siguientes pasos

# Fases de la reunión

## Grupos de conceptualización

Cada persona dinamizadora tendrá un espacio para trabajar con un grupo
de personas durante un tiempo limitado para conceptualizar (recoger
opiniones e ideas) sobre el tema del que es responsable.

Cada cierto tiempo, las personas del grupo serán obligadas a rotar a
otro grupo.

Como resultado de este fase, pueden surgir nuevos grupos para las
siguientes fases.

## Grupos de desarrollo

Cada grupo trabajará sobre los conceptos definidos en la fase anterior para buscar desarrollar el tema hacia su resolución

## Grupos de terminación

Cada grupo generará un resumen de las lecciones aprendidas sobre el
tema, resoluciones, y siguientes pasos

Los resultados se pondrán a disposición de la comunidad en [nuestro GitLab](https://gitlab.com/devrel-aguacates) 

# Materiales

- 1 micrófono?
- Post-its
- Pizarras o paneles donde escribir

