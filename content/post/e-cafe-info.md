+++
author = "Aguacate anónimo"
title = "Aguacate Café"
date = "2020-02-11"
description = "Agenda e información útil"
tags = [
    "cafe",
]
+++

La comunidad de DevRel Aguacates se reune cada 2 jueves de forma
"virtual" a través de una videollamada para tratar distintos temas
relacionados con la actividad de DevRel de las personas que la componen.

Tenemos un [repositorio](https://gitlab.com/devrel-aguacates/reuniones)
que recoge las notas de las reuniones que vaya teniendo el grupo, con
toda la información relevante que pueda surgir durante la conversación.

Como siempre, nos coordinamos por [Slack](https://devrel-espana.slack.com).
