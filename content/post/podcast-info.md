+++
author = "Aguacate anónimo"
title = "Lanzamos el Podcast DevRel España"
date = "2020-02-12"
description = "Lanzamos un podcast!"
tags = [
    "podcast",
]
+++

Como parece que tenemos pocas cosas que hacer, nos metemos en otro nuevo jardín: un __podcast__!

*¿Cómo? ¡Qué interesante! ¡Cuéntame más!*

__¿Qué temática tendrá?__

Hablaremos de comunidades, tecnología, open source, de lo humano y lo
divino. Todo ello atado bien corto por la férrea mano de María Encinar.

__¿Qué periodicidad tendrá?__

Atemporal, como el vino. 

__¿Dónde puedo descargarlo?__

Aún está por decididir. Conociéndonos, seguramente vía `wget`. Al tiempo.

__Me encantaría participar, ¿Puedo?__

¡Claro! Pásate por [Slack](https://devrel-espana.slack.com) y nos cuentas.


